const books = [
  { title: 'Title1', author: 'Author1', genre: 'Genre1', reviews: [{ score: 5, message: 'Message' }], price: 50 },
  { title: 'Title2', author: 'Author2', genre: 'Genre2', reviews: [{ score: 5, message: 'Message' }], price: 60 },
];

try {
  print('Initializing Book DB');
  const res = [db.books.drop(), db.books.createIndexes([{ author: 1 }, { genre: 1 }]), db.books.insert(books)];
  printjson(res);
} catch (error) {
  print(error);
}
