const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const expect = chai.expect;

describe('IDP Cart Manager Service', () => {
  describe('Get Cart', () => {
    it('When the user is signed in and the route is called, then the route should respond with status 200', async () => {
      const credentials = { username: 'username_c1', password: 'password' };

      const res1 = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);
      const token = res1.body.token;
      const res2 = await chai.request('http://idp-cart-manager-service:8080').get('/api/cart').set('authtoken', token);

      expect(res2).to.have.status(200);
    });
  });
  describe('Post Cart Add', () => {
    it('When the user is signed in and the body contains an item, then the route should respond with status 200', async () => {
      const credentials = { username: 'username_c2', password: 'password' };
      const item = { item: { _id: 'mockitem', amount: 1 } };

      const res1 = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);
      const token = res1.body.token;
      const res2 = await chai
        .request('http://idp-cart-manager-service:8080')
        .post('/api/cart/add')
        .set('authtoken', token)
        .send(item);

      expect(res2).to.have.status(200);
    });
  });
  describe('Post Cart Rm', () => {
    it('When the user is signed in, the cart contains an item and the body contains the same item, then the route should respond with status 200', async () => {
      const credentials = { username: 'username_c3', password: 'password' };
      const item = { item: { _id: 'mockitem', amount: 1 } };

      const res1 = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);
      const token = res1.body.token;
      const res2 = await chai
        .request('http://idp-cart-manager-service:8080')
        .post('/api/cart/add')
        .set('authtoken', token)
        .send(item);
      const res3 = await chai
        .request('http://idp-cart-manager-service:8080')
        .post('/api/cart/rm')
        .set('authtoken', token)
        .send(item);

      expect(res3).to.have.status(200);
    });
  });
  describe('Delete Cart', () => {
    it('When the user is signed in and the route is called, then the route should respond with status 200', async () => {
      const credentials = { username: 'username_c4', password: 'password' };

      const res1 = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);
      const token = res1.body.token;
      const res2 = await chai
        .request('http://idp-cart-manager-service:8080')
        .delete('/api/cart')
        .set('authtoken', token);

      expect(res2).to.have.status(200);
    });
  });
});
