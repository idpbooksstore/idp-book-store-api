const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const expect = chai.expect;

describe('IDP Checkout Service', () => {
  describe('Post Checkout', () => {
    it('When the user is signed in, the cart is not empty and the route is called, then the route should respond with status 200', async () => {
      const res1 = await chai.request('http://idp-books-service:8080').get('/api/books');
      const credentials = { username: 'username_h1', password: 'password' };
      const item = { item: { _id: res1.body.books[0]._id, amount: 1 } };
      const checkout = { address: 'address', card: { card: '4242424242424242', cvc: '1234', exp: '02/2100' } };

      const res2 = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);
      const token = res2.body.token;
      const res3 = await chai
        .request('http://idp-cart-manager-service:8080')
        .post('/api/cart/add')
        .set('authtoken', token)
        .send(item);
      const res4 = await chai
        .request('http://idp-checkout-service:8080')
        .post('/api/checkout')
        .set('authtoken', token)
        .send(checkout);

      expect(res4).to.have.status(200);
    });
  });
});
