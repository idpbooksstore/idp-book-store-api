const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const expect = chai.expect;

describe('IDP Auth Service', () => {
  describe('Post Register', () => {
    it('When the body contains username and password, then the route should respond with an auth token', async () => {
      const credentials = { username: 'username_a1', password: 'password' };

      const res = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);

      expect(res).to.have.status(200);
    });
  });
  describe('Post Auth', () => {
    it('When the body contains username and password and the user exists, then the route should respond with an auth token', async () => {
      const credentials = { username: 'username_a2', password: 'password' };

      const res1 = await chai.request('http://idp-auth-service:8080').post('/api/auth/register').send(credentials);
      const res2 = await chai.request('http://idp-auth-service:8080').post('/api/auth/login').send(credentials);

      expect(res2).to.have.status(200);
    });
  });
});
