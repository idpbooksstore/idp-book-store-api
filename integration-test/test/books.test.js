const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const expect = chai.expect;

describe('IDP Books Service', () => {
  describe('Get Books', () => {
    it('When the query is empty, then the books route should return all the books', async () => {
      const res = await chai.request('http://idp-books-service:8080').get('/api/books');

      expect(res).to.have.status(200);
    });
  });
  describe('Get Filters', () => {
    it('When the route is accessed, then all the filters should be returned', async () => {
      const res = await chai.request('http://idp-books-service:8080').get('/api/books/filters');

      expect(res).to.have.status(200);
    });
  });
});
