# Online Book Store

## Objective

In this project, our main goal is to implement the backend API of an online book store. The main features of this REST API are the ability to visualize and filter books, create an account, add items to the shopping cart and make payments online.

## Architecture

This project is using microservices and docker-compose / stack to deploy.

![alt text](./resources/arch.png)

## Getting Started

#### Docker Compose

##### Tested using:
* Ubuntu 20.10
* Docker version 20.10.5, build 55c4c88
* docker-compose version 1.28.5, build c4eb3a1f

##### Requirements:
In order to run this container you'll need docker installed.
* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

Additionally Docker Compose is needed for this deployment method.
* [Install](https://docs.docker.com/compose/install/)

##### Usage
1. Download the repository.
```shell
$ git clone https://gitlab.com/idpbooksstore/idp-book-store-api.git
$ cd idp-book-store-api
```
2. Change the default secrets [./secrets](./secrets).
3. Additionally you can change the default book set [./idp-books-db/initdb.js](./idp-books-db/initdb.js).
4. Start the application using the configuration file [./docker-compose.yml](./docker-compose.yml). This will pull the latest images used for this project.
```shell
$ docker-compose up
```
5. The server will run on `localhost:8000` and will respond to the routes exposed by Kong [./kong/kong.yml](./kong/kong.yml).
6. Portainer is used for admin task and it is available at `localhost:9000`.
7. For database management the stack uses mongo-express running on ports `8081`, `8082`, `8083`, `8084` at `localhost`.
8. To stop the application and delete the volumes run:
```shell
$  docker-compose down --volumes
```

#### Docker Swarm

##### Tested using:
* [play-with-docker](https://labs.play-with-docker.com/)

##### Requirements:
In order to run this container you'll need docker installed.
* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

Additionally, to run on the local machine you will need Docker Machine installed.
* [Install](https://docs.docker.com/machine/install-machine/)

##### Usage
* [play-with-docker](https://labs.play-with-docker.com/)
  1. Create a new instance (+ADD NEW INSTANCE)
  2. Download the repository.
  ```shell
  $ git clone https://gitlab.com/idpbooksstore/idp-book-store-api.git
  $ cd idp-book-store-api
  ```
  3. Change the default secrets [./secrets](./secrets).
  4. Additionally you can change the default book set [./idp-books-db/initdb.js](./idp-books-db/initdb.js).
  5. Initialize the swarm. If the command does not work use the flag `--advertise-addr` with one of the available ip addresses (Use the one starting with `172` if available).
  ```shell
  $ docker swarm init
  ```
  6. Deploy the stack using the configuration file provided in the repository [./docker-stack.yml](./docker-stack.yml) and the name `idp`. This will pull the latest images used for this project.
  ```shell
  $ docker stack deploy -c docker-stack.yml idp
  ```
  7. The server URI is allocated by play with docker (click on the `8000` link and copy the URI corresponding to port `8000`, you will no longer need to specify the port when using this URI) and will respond to the routes exposed by Kong [./kong/kong.yml](./kong/kong.yml).
  8. Portainer is used for admin task and it is available on port `9000`, using the URI provided by play with docker.
  9. For database management the stack uses mongo-express running on ports `8081`, `8082`, `8083`, `8084`, using the URI provided by play with docker.
  10. To stop the stack use the following command.
  ```shell
  $  docker stack rm idp
  ```
  11. To leave the swarm use the following command (use the flag `--force` to force the command).
  ```shell
  $ docker swarm leave
  ```
* [play-with-docker](https://labs.play-with-docker.com/) using templates
  1. Create a new swarm using a template (e.g 3 managers and 2 workers).
  2. Use the leader node in case of multiple managers.
  ```shell
  $ docker node ls
  ```
  3. Download the repository.
  ```shell
  $ git clone https://gitlab.com/idpbooksstore/idp-book-store-api.git
  $ cd idp-book-store-api
  ```
  4. Change the default secrets [./secrets](./secrets).
  5. Additionally you can change the default book set [./idp-books-db/initdb.js](./idp-books-db/initdb.js).
  6. Deploy the stack using the configuration file provided in the repository [./docker-stack.yml](./docker-stack.yml) and the name `idp`. This will pull the latest images used for this project.
  ```shell
  $ docker stack deploy -c docker-stack.yml idp
  ```
  7. The server URI is allocated by play with docker (click on the `8000` link and copy the URI corresponding to port `8000`, you will no longer need to specify the port when using this URI) and will respond to the routes exposed by Kong [./kong/kong.yml](./kong/kong.yml).
  8. Portainer is used for admin task and it is available on port `9000`, using the URI provided by play with docker.
  9. For database management the stack uses mongo-express running on ports `8081`, `8082`, `8083`, `8084`, using the URI provided by play with docker.
  10. To stop the stack use the following command.
  ```shell
  $  docker stack rm idp
  ```
  11. To leave the swarm use the following command (use the flag `--force` to force the command).
  ```shell
  $ docker swarm leave
  ```

## Routes exposed by Kong

- `GET /api/books?id[]=string&genre[]=string&author[]=string` - get the books based on filters.
- `GET /api/books/filters` - get the available filters.
- `GET /api/cart` - get the shopping cart for an user.
- `POST /api/cart/add` - add an item to the shopping cart of an user.
- `POST /api/cart/rm` - remove an item from the shopping cart of an user.
- `DELETE /api/cart` - clear the shopping cart of an user.
- `POST /api/auth/register` - create an account.
- `POST /api/auth/login` - start a session for an user.
- `POST /api/checkout` - make a payment and request a shipping for a user.

## Internal REST API

### Books
- `GET /api/books?id[]=string&genre[]=string&author[]=string`
  - Get the books based on filters send as query parameters.
  - Output:
  ```typescript
  {
    message: string,
    books: [{_id: string, title: string, author: string, genre: string, reviews: [{score: number, message: string}], price: number}]
  }
  ```
- `GET /api/books/filters`
  - Get the available filters
  - Output:
  ```typescript
  {
    message: string,
    filters: {author: string[], genre: string[]}
  }
  ```

### CartManager
- `GET /api/cart`
  - Get the shopping cart for an user.
  - Header: The token obtained during authentication that will be used to identify the user.
  ```typescript
  {token: string}
  ```
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```
- `POST /api/cart/add`
  - Add an item to the shopping cart of an user, if the header contains a valid token.
  - Header: The token obtained during authentication that will be used to identify the user.
  ```typescript
  {token: string}
  ```
  - Body: The item that will be added to the shopping cart.
  ```typescript
  {item: {_id: string, amount: number}
  ```
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```
- `POST /api/cart/rm`
  - Delete an item from the shopping cart.
  - Header: The token obtained during authentication that will be used to identify the user.
  ```typescript
  {token: string}
  ```
  - Body: The item that will be deleted from the shopping cart.
  ```typescript
  {item: {_id: string, amount: number}
  ```
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```
- `DELETE /api/cart`
  - Clear the shopping cart
  - Header: The token obtained during authentication that will be used to identify the user.
  ```typescript
  {token: string}
  ```
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```

### CartStore
- `GET /api/cart/store/:user`
  - Get the shopping cart of the user with the id received as a URI parameter.
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```
- `POST /api/cart/add`
  - Add an item to the shopping cart.
  - Body: the user id and item that will be added to the shopping cart.
  ```typescript
  {user: string, item: {_id: string, amount: number}}
  ```
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```
- `POST /api/cart/rm`
  - Delete an item from the shopping cart
  - Body: the user id and item that will be delted from the shopping cart.
  ```typescript
  {user: string, item: {_id: string, amount: number}}
  ```
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```
- `DELETE /api/cart/store/:user`
  - Clear the shopping cart using the user id received as an URI parameter
  - Output:
  ```typescript
  {
    message: string,
    items: [{_id: string, amount: number}]
  }
  ```

### Auth
- `POST /api/auth/register`
  - Create a new account and authenticate the new user.
  - Body: the information required to create an account.
  ```typescript
  {username: string, password: string, email: string}
  ```
  - Output:
  ```typescript
  {
    message: string
    token: string
  }
  ```
- `POST /api/auth/login`
  - Authenticate the user.
  - Body: username and password.
  ```typescript
  {username: string, password: string}
  ```
  - Output:
  ```typescript
  {
    message: string,
    token: string
  }
  ```

### Session
- `GET /api/session/verify/:token`
  - Verify if a token is valid.
  - URI parameter: the token that will be verified.
  ```typescript
  {token: string}
  ```
  - Output:
  ```typescript
  {
    message: string,
    user: string
  }
  ```
- `GET /api/session/sign/:user`
  - Generate a new token based on a user id.
  - URI parameter: the user id that will be used to sign a new token.
  ```typescript
  {user: string}
  ```
  - Output:
  ```typescript
  {
    message: string,
    token: string
  }
  ```

### Checkout
- `POST /api/checkout`
  - This microservice uses multiple other services to create a payment and create a shipping request. First the service will verify that the user is authenticated using the token received in the header. Then it will fetch the user's shopping cart and calculate the total price. Afterwards, it will make a request to the payment service and if that is successful it will make a request to the shipping microservice.
  - Header: The token used by the authentication service to generate a session.
  ```typescript
  {token: string}
  ```
  - Body: The address and the credit card of the user
  ```typescript
  {address: string, card: {card: string, cvc: number, date: string}}
  ```
  - Output:
  ```typescript
  {
    message: string
  }
  ```

### Payment
- `POST /api/payment`
  - Makes a transaction using an external API
  - Body:
  ```typescript
  {card: {card: string, cvc: number, date: string}, amount: number}
  ```
  - Output:
  ```typescript
  {
    message: string
  }
  ```

### Shipping
- `POST /api/shipping`
  - Makes a shipping request.
  - Body:
  ```typescript
  {items: [{_id: string, amount: number}], address: string}
  ```
  - Output:
  ```typescript
  {
    shipping: ShippingData
    message: string
  }
  ```

### PaymentLogger >>> extra
- `GET /api/log`
- `POST /api/log/add`
  - Body:
  ```typescript
  {user: string, products: [{_id: string, amount: number}]}
  ```
  - Output:
  ```typescript
  {
    message: string
  }
  ```

  ### Recommendation >>> extra
- `GET /api`
