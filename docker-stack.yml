version: "3.8"

services:
  kong:
    image: kong:latest
    restart: always
    volumes:
      - ./kong:/usr/local/kong/declarative
    environment:
      KONG_DATABASE: "off"
      KONG_DECLARATIVE_CONFIG: /usr/local/kong/declarative/kong.yml
      KONG_PROXY_ACCESS_LOG: /dev/stdout
      KONG_ADMIN_ACCESS_LOG: /dev/stdout
      KONG_PROXY_ERROR_LOG: /dev/stderr
      KONG_ADMIN_ERROR_LOG: /dev/stderr
      KONG_ADMIN_LISTEN: 0.0.0.0:8001, 0.0.0.0:8444 ssl
    ports:
      - 8000:8000
      - 8443:8443
      - 8001:8001
      - 8444:8444
    networks:
      - books_api-kong-net
      - cart_manager_api-kong-net
      - auth_api-kong-net
      - checkout_api-kong-net
    deploy:
      placement:
        constraints:
          - "node.role==manager"

  portainer:
    image: portainer/portainer-ce
    restart: always
    command: -H unix:///var/run/docker.sock
    ports:
      - 9000:9000
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - idp-portainer-data:/data
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - "node.platform.os==linux"
          - "node.role==manager"

  idp-books-service:
    image: alexjercan/idp-books:latest
    restart: always
    user: "root"
    environment:
      PORT: 8080
      MONGO_HOST: idp-books-db
      MONGO_DATABASE: book
      MONGO_USERNAME_FILE: /run/secrets/idp-books-mongo-username
      MONGO_PASSWORD_FILE: /run/secrets/idp-books-mongo-password
    secrets:
      - idp-books-mongo-username
      - idp-books-mongo-password
    networks:
      - books_api-kong-net
      - books_api-checkout_api-net
      - books_api-cart_manager_api-net
      - db-books_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  # idp-books-admin:
  #   image: mongo-express:latest
  #   environment:
  #     ME_CONFIG_MONGODB_SERVER: idp-books-db
  #     ME_CONFIG_MONGODB_ADMINUSERNAME_FILE: /run/secrets/idp-books-mongo-username
  #     ME_CONFIG_MONGODB_ADMINPASSWORD_FILE: /run/secrets/idp-books-mongo-password
  #   secrets:
  #     - idp-books-mongo-username
  #     - idp-books-mongo-password
  #   ports:
  #     - 8081:8081
  #   networks:
  #     - db-books_admin-net

  idp-books-db:
    image: mongo:latest
    environment:
      MONGO_INITDB_ROOT_USERNAME_FILE: /run/secrets/idp-books-mongo-username
      MONGO_INITDB_ROOT_PASSWORD_FILE: /run/secrets/idp-books-mongo-password
      MONGO_INITDB_DATABASE: book
    secrets:
      - idp-books-mongo-username
      - idp-books-mongo-password
    volumes:
      - ./idp-books-db:/docker-entrypoint-initdb.d:ro
      - idp-books-volume:/data/db
    networks:
      - db-books_api-net
      - db-books_admin-net
    deploy:
      placement:
        constraints:
          - "node.role==manager"

  idp-cart-manager-service:
    image: alexjercan/idp-cart-manager:latest
    restart: always
    environment:
      PORT: 8080
      BOOKS_HOST: idp-books-service
      BOOKS_PORT: 8080
      CART_STORE_HOST: idp-cart-store-service
      CART_STORE_PORT: 8080
      SESSION_HOST: idp-session-service
      SESSION_PORT: 8080
      AUTH_TOKEN_FILE: /run/secrets/idp-auth-token
    secrets:
      - idp-auth-token
    networks:
      - cart_manager_api-kong-net
      - books_api-cart_manager_api-net
      - cart_store_api-cart_manager_api-net
      - session_api-cart_manager_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  idp-cart-store-service:
    image: alexjercan/idp-cart-store:latest
    restart: always
    environment:
      PORT: 8080
      MONGO_HOST: idp-cart-db
      MONGO_DATABASE: cart
      MONGO_USERNAME_FILE: /run/secrets/idp-cart-mongo-username
      MONGO_PASSWORD_FILE: /run/secrets/idp-cart-mongo-password
    secrets:
      - idp-cart-mongo-username
      - idp-cart-mongo-password
    networks:
      - cart_store_api-cart_manager_api-net
      - cart_store_api-checkout_api-net
      - db-cart_store_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  # idp-cart-admin:
  #   image: mongo-express:latest
  #   environment:
  #     ME_CONFIG_MONGODB_SERVER: idp-cart-db
  #     ME_CONFIG_MONGODB_ADMINUSERNAME_FILE: /run/secrets/idp-cart-mongo-username
  #     ME_CONFIG_MONGODB_ADMINPASSWORD_FILE: /run/secrets/idp-cart-mongo-password
  #   secrets:
  #     - idp-cart-mongo-username
  #     - idp-cart-mongo-password
  #   ports:
  #     - 8082:8081
  #   networks:
  #     - db-cart_store_admin-net

  idp-cart-db:
    image: mongo:latest
    environment:
      MONGO_INITDB_ROOT_USERNAME_FILE: /run/secrets/idp-cart-mongo-username
      MONGO_INITDB_ROOT_PASSWORD_FILE: /run/secrets/idp-cart-mongo-password
      MONGO_INITDB_DATABASE: cart
    secrets:
      - idp-cart-mongo-username
      - idp-cart-mongo-password
    volumes:
      - idp-cart-volume:/data/db
    networks:
      - db-cart_store_api-net
      - db-cart_store_admin-net
    deploy:
      placement:
        constraints:
          - "node.role==manager"

  idp-auth-service:
    image: alexjercan/idp-auth:latest
    restart: always
    environment:
      PORT: 8080
      MONGO_HOST: idp-auth-db
      MONGO_DATABASE: user
      MONGO_USERNAME_FILE: /run/secrets/idp-auth-mongo-username
      MONGO_PASSWORD_FILE: /run/secrets/idp-auth-mongo-password
      SESSION_HOST: idp-session-service
      SESSION_PORT: 8080
    secrets:
      - idp-auth-mongo-username
      - idp-auth-mongo-password
    networks:
      - auth_api-kong-net
      - session_api-auth_api-net
      - db-auth_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  idp-session-service:
    image: alexjercan/idp-session:latest
    restart: always
    environment:
      PORT: 8080
    networks:
      - session_api-cart_manager_api-net
      - session_api-auth_api-net
      - session_api-checkout_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  # idp-auth-admin:
  #   image: mongo-express:latest
  #   environment:
  #     ME_CONFIG_MONGODB_SERVER: idp-auth-db
  #     ME_CONFIG_MONGODB_ADMINUSERNAME_FILE: /run/secrets/idp-auth-mongo-username
  #     ME_CONFIG_MONGODB_ADMINPASSWORD_FILE: /run/secrets/idp-auth-mongo-password
  #   secrets:
  #     - idp-auth-mongo-username
  #     - idp-auth-mongo-password
  #   ports:
  #     - 8083:8081
  #   networks:
  #     - db-auth_admin-net

  idp-auth-db:
    image: mongo:latest
    environment:
      MONGO_INITDB_ROOT_USERNAME_FILE: /run/secrets/idp-auth-mongo-username
      MONGO_INITDB_ROOT_PASSWORD_FILE: /run/secrets/idp-auth-mongo-password
      MONGO_INITDB_DATABASE: auth
    secrets:
      - idp-auth-mongo-username
      - idp-auth-mongo-password
    volumes:
      - idp-auth-volume:/data/db
    networks:
      - db-auth_api-net
      - db-auth_admin-net
    deploy:
      placement:
        constraints:
          - "node.role==manager"

  idp-checkout-service:
    image: alexjercan/idp-checkout:latest
    restart: always
    environment:
      PORT: 8080
      BOOKS_HOST: idp-books-service
      BOOKS_PORT: 8080
      CART_STORE_HOST: idp-cart-store-service
      CART_STORE_PORT: 8080
      SESSION_HOST: idp-session-service
      SESSION_PORT: 8080
      PAYMENT_HOST: idp-payment-service
      PAYMENT_PORT: 8080
      SHIPPING_HOST: idp-shipping-service
      SHIPPING_PORT: 8080
      AUTH_TOKEN_FILE: /run/secrets/idp-auth-token
    secrets:
      - idp-auth-token
    networks:
      - checkout_api-kong-net
      - books_api-checkout_api-net
      - cart_store_api-checkout_api-net
      - session_api-checkout_api-net
      - payment_api-checkout_api-net
      - shipping_api-checkout_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  idp-payment-service:
    image: alexjercan/idp-payment:latest
    restart: always
    environment:
      PORT: 8080
    networks:
      - payment_api-checkout_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  idp-shipping-service:
    image: alexjercan/idp-shipping:latest
    restart: always
    environment:
      PORT: 8080
      MONGO_HOST: idp-shipping-db
      MONGO_DATABASE: shipping
      MONGO_USERNAME_FILE: /run/secrets/idp-shipping-mongo-username
      MONGO_PASSWORD_FILE: /run/secrets/idp-shipping-mongo-password
    secrets:
      - idp-shipping-mongo-username
      - idp-shipping-mongo-password
    networks:
      - shipping_api-checkout_api-net
      - db-shipping_api-net
    deploy:
      replicas: 1
      placement:
        max_replicas_per_node: 2
      update_config:
        order: start-first
      rollback_config:
        order: stop-first

  idp-shipping-admin:
    image: mongo-express:latest
    environment:
      ME_CONFIG_MONGODB_SERVER: idp-shipping-db
      ME_CONFIG_MONGODB_ADMINUSERNAME_FILE: /run/secrets/idp-shipping-mongo-username
      ME_CONFIG_MONGODB_ADMINPASSWORD_FILE: /run/secrets/idp-shipping-mongo-password
    secrets:
      - idp-shipping-mongo-username
      - idp-shipping-mongo-password
    ports:
      - 8084:8081
    networks:
      - db-shipping_admin-net

  idp-shipping-db:
    image: mongo:latest
    environment:
      MONGO_INITDB_ROOT_USERNAME_FILE: /run/secrets/idp-shipping-mongo-username
      MONGO_INITDB_ROOT_PASSWORD_FILE: /run/secrets/idp-shipping-mongo-password
      MONGO_INITDB_DATABASE: shipping
    secrets:
      - idp-shipping-mongo-username
      - idp-shipping-mongo-password
    volumes:
      - idp-shipping-volume:/data/db
    networks:
      - db-shipping_api-net
      - db-shipping_admin-net
    deploy:
      placement:
        constraints:
          - "node.role==manager"

volumes:
  idp-books-volume:
  idp-cart-volume:
  idp-auth-volume:
  idp-shipping-volume:
  idp-portainer-data:

networks:
  books_api-kong-net:
  books_api-checkout_api-net:
  books_api-cart_manager_api-net:
  db-books_api-net:
  db-books_admin-net:

  cart_manager_api-kong-net:
  cart_store_api-cart_manager_api-net:
  cart_store_api-checkout_api-net:
  db-cart_store_api-net:
  db-cart_store_admin-net:

  auth_api-kong-net:
  session_api-auth_api-net:
  session_api-cart_manager_api-net:
  session_api-checkout_api-net:
  db-auth_api-net:
  db-auth_admin-net:

  checkout_api-kong-net:
  payment_api-checkout_api-net:
  shipping_api-checkout_api-net:
  db-shipping_api-net:
  db-shipping_admin-net:

secrets:
  idp-books-mongo-username:
    file: ./secrets/idp-books-mongo-username
  idp-books-mongo-password:
    file: ./secrets/idp-books-mongo-password
  idp-cart-mongo-username:
    file: ./secrets/idp-cart-mongo-username
  idp-cart-mongo-password:
    file: ./secrets/idp-cart-mongo-password
  idp-auth-mongo-username:
    file: ./secrets/idp-auth-mongo-username
  idp-auth-mongo-password:
    file: ./secrets/idp-auth-mongo-password
  idp-shipping-mongo-username:
    file: ./secrets/idp-shipping-mongo-username
  idp-shipping-mongo-password:
    file: ./secrets/idp-shipping-mongo-password
  idp-auth-token:
    file: ./secrets/idp-auth-token
